gen-auth:
	protoc \
    -I proto \
    proto/auth/auth.proto \
    --go_out=./gen/go \
    --go_opt=paths=source_relative \
    --go-grpc_out=./gen/go  \
    --go-grpc_opt=paths=source_relative


gen-profile:
	protoc \
    	-I proto \
    	proto/profile/profile.proto \
    	--go_out=./gen/go \
    	--go_opt=paths=source_relative \
    	--go-grpc_out=./gen/go  \
    	--go-grpc_opt=paths=source_relative

gen-resource:
	protoc \
		-I proto \
        proto/resource/resource.proto \
        --go_out=./gen/go \
        --go_opt=paths=source_relative \
        --go-grpc_out=./gen/go  \
        --go-grpc_opt=paths=source_relative